import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import verbenMitPrapositionenReducer from '../pages/prapositionen/verben-mit-prapositionenSlice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    verbenMitPrapositionen: verbenMitPrapositionenReducer
  },
});
