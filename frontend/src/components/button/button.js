import React, { Component } from 'react';
import './button.scss';

// possible properties:
// color: red, yellow, green, white, blue, yellow, brown

const Button = (props) => {

    return (<div className='button-component'>
        <input
            className={props.color != undefined ? `button ${props.color}` : 'button'}
            type='button'
            value={props.value != undefined ? props.value : 'button'}
            onClick={props.onClick}
            width='10px'
        />
    </div>);
}

export default Button;