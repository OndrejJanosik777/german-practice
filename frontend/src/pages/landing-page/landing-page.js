import React, { Component } from 'react';
import { Navigate } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import './landing-page.scss';

const MainPage = () => {
    const navigate = useNavigate();

    return (<div className='landing-page'>
        {/* <img className='background' src="https://images.pexels.com/photos/65567/pexels-photo-65567.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="" /> */}
        <div className='faked-opacity'></div>
        <div className='options'>
            <input type='button' className='button-main' value='verben' />
            <input type='button' className='button-main' value='nomen und artikeln' />
            <input type='button' className='button-main' value='pronomen' />
            <input type='button' className='button-main' value='adjektive' />
            <input type='button' className='button-main' value='Präpositionen' onClick={() => { navigate("/prapositionen/") }} />
            <input type='button' className='button-main' value='Adverbien und Artikeln' />
            <input type='button' className='button-main' value='Einfache Sätze' />
            <input type='button' className='button-main' value='Zusammengesetzte Sätze' />
        </div>
    </div>);
}

export default MainPage;