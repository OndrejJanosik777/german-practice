import React, { Component } from 'react';
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import './overview.scss';
import Button from '../../../components/button/button';

const Overview = () => {
    // get values from redux state
    const allTasks = useSelector(state => state.verbenMitPrapositionen.all);
    const navigate = useNavigate();

    return (<div className='overview'>
        <div className='faked-opacity'></div>
        <div className='header'>
            <div className='navbar'>
                <Button color='blue' value='<< PRÄPOSITIONEN' onClick={() => navigate("/prapositionen/")} />
            </div>
        </div>
        <div className='main'>
            <div className='main-header'>
                <div className='text'>Overview <em>"Verben mit Präpositionen"</em>:</div>
            </div>
            <div className='content'>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Verb</th>
                            <th>Präposition</th>
                            <th>Kasus</th>
                            <th>Beispiel</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allTasks.map((task) => {
                            return <tr>
                                <td>{task[0].verb}</td>
                                <td>{task[0].praposition}</td>
                                <td>+ {task[0].kasus}</td>
                                <td>{task[0].beispiel}</td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
            <div className='button-container'>
                <Button color='green' value='TRY IT YOURSELF' onClick={() => navigate("/prapositionen/practice/")} />
            </div>
        </div>
        {/* <div className='temporary'>
            <strong>Redux state:</strong>
            <div onClick={() => { console.log('allTasks: ', allTasks) }}>allTasks</div>
        </div> */}
    </div>);
}

export default Overview;