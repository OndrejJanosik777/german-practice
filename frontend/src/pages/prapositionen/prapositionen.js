import React, { Component } from 'react';
import './prapositionen.scss';
import { useNavigate } from "react-router-dom";
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { chooseTasks, setMode, zeroTasks } from './verben-mit-prapositionenSlice';
import Button from '../../components/button/button';


const Prapositionen = () => {
    // get values from redux state
    const allTasks = useSelector(state => state.verbenMitPrapositionen.all);
    const dispatch = useDispatch();

    // state
    const [NrOfExercises, setNrOfExercises] = useState(30);
    const [verbenMitPrapositionenExpanded, setVerbenMitPrapositionenExpanded] = useState(true)

    const navigate = useNavigate();

    const btnPractice_Click = () => {
        // console.log('button practise clicked...');

        let rbtnPractice = document.getElementById('practice-mode');
        let rbtnExam = document.getElementById('exam-mode');

        if (rbtnPractice.checked) { dispatch(setMode('practice-mode')); }
        if (rbtnExam.checked) { dispatch(setMode('exam-mode')); }

        dispatch(zeroTasks());
        dispatch(chooseTasks(NrOfExercises));

        navigate("/prapositionen/practice/");
    }

    const btnExplanation_Click = () => { console.log('button Explanation clicked...'); }

    const btnRules_Click = () => { console.log('button Rules clicked...'); }

    const btnOverview_Click = () => { console.log('button Overview clicked...'); }

    const btnBackToMenu_Click = () => { navigate("/"); }

    const radio_change = () => {
        console.log('radio practice changed...');

        let rbtnPractice = document.getElementById('practice-mode');
        let rbtnExam = document.getElementById('exam-mode');
        // console.log('practice-mode checked: ', rbtnPractice.checked);
        // console.log('exam-mode checked: ', rbtnExam.checked);
    }

    return (<div className='prapositionen'>
        <div className='faked-opacity'></div>
        <div className='header'>
            <div className='navbar'>
                <Button color='blue' value='<< BACK TO MENU' onClick={btnBackToMenu_Click} />
            </div>
        </div>
        <div className='main' >
            <div className='main-header' onClick={() => { navigate("/prapositionen/overview/") }}>
                <div className='text'>
                    <div></div>
                    <em>Overview (Verben mit Präpositionen)</em>
                    <div></div>
                </div>
            </div>
            <div className='main-header' onClick={() => { setVerbenMitPrapositionenExpanded(!verbenMitPrapositionenExpanded); }}>
                <div className='text'>
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-caret-down" viewBox="0 0 16 16">
                        <path
                            d={verbenMitPrapositionenExpanded
                                ?
                                "M3.204 5h9.592L8 10.481 3.204 5zm-.753.659 4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"
                                :
                                "M3.204 11h9.592L8 5.519 3.204 11zm-.753-.659 4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"
                            }
                        />
                    </svg>
                    <em>Exercise (Verben mit Präpositionen)</em>
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-caret-down" viewBox="0 0 16 16">
                        <path
                            d={verbenMitPrapositionenExpanded
                                ?
                                "M3.204 5h9.592L8 10.481 3.204 5zm-.753.659 4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z"
                                :
                                "M3.204 11h9.592L8 5.519 3.204 11zm-.753-.659 4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z"
                            }
                        />
                    </svg>
                </div>
            </div>
            <div className={verbenMitPrapositionenExpanded ? 'center' : 'center-hidden'}>
                <div className='row'>
                    <div className='text-25'>Number of exercises: </div>
                    <div className='group'>
                        <input className='input-field' type='number' min={1} max={allTasks.length} value={NrOfExercises} onChange={(e) => setNrOfExercises(e.target.value)} />
                        <div className='text-25'>max. {allTasks.length}</div>
                    </div>
                </div>
                <div className='row'>
                    <div className='text-25'>Mode: </div>
                    <div className='group'>
                        <input className='radio-mark' type='radio' id='practice-mode' name='mode' value='practice-mode' onClick={radio_change} checked />
                        <label className='radio-label' htmlFor='practice-mode' >practice-mode</label>
                    </div>
                    <div className='group'>
                        <input className='radio-mark' type='radio' id='exam-mode' onClick={radio_change} name='mode' value='exam-mode' />
                        <label className='radio-label' htmlFor='exam-mode' >exam-mode</label>
                    </div>
                </div>
                <div className='buttons'>
                    <Button color='light-green' value='PRACTISE' onClick={btnPractice_Click} />
                    {/* <Button color='light-blue' value='EXPLANATION' onClick={btnExplanation_Click} /> */}
                    {/* <Button color='light-blue' value='RULES' onClick={btnRules_Click} /> */}
                    {/* <Button color='light-blue' value='OVERVIEW' onClick={btnOverview_Click} /> */}
                </div>
            </div>
        </div>
    </div>);
}

export default Prapositionen;