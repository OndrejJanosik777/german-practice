import { createSlice } from '@reduxjs/toolkit'

export const verbenMitPrapositionenSlice = createSlice({
    name: 'verbenMitPrapositionen',
    initialState: {
        // all tasks for verben mir prapositionen practise
        all: [
            [
                {
                    verb: 'abhängen',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Ob wir fahren, hängt vom Wetter ab.',
                }
            ],
            [
                {
                    verb: 'achten',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Bitte achte auf den neuen Mantel.',
                }
            ],
            [
                {
                    verb: 'anfangen',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Ich fange mit der Übung an.',
                }
            ],
            [
                {
                    verb: 'ankommen',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Es kommt auf den richtigen Preis an.',
                }
            ],
            [
                {
                    verb: 'antworten',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Bitte antworten Sie heute auf den Brief.',
                }
            ],
            [
                {
                    verb: 'sich ärgern',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Wir ärgern uns über den Regen.',
                }
            ],
            [
                {
                    verb: 'aufhören',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Er hört um 17.00 Uhr mit der Arbeit auf.',
                }
            ],
            [
                {
                    verb: 'aufpassen',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Ein Babysitter passt auf kleine Kinder auf.',
                }
            ],
            [
                {
                    verb: 'sich aufregen',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Deutsche regen sich über Unpünktlichkeit auf.',
                }
            ],
            [
                {
                    verb: 'ausgeben',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Manche geben viel Geld für Schuhe aus.',
                }
            ],
            [
                {
                    verb: 'sich bedanken',
                    praposition: 'bei',
                    kasus: 'D',
                    beispiel: 'Ich bedanke mich herzlich bei dir',
                }
            ],
            [
                {
                    verb: 'sich bedanken',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Martin bedankt sich für das Geschenk.',
                }
            ],
            [
                {
                    verb: 'beginnen',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Wir beginnen pünktlich mit dem Deutschkurs.',
                }
            ],
            [
                {
                    verb: 'sich bemühen',
                    praposition: 'um',
                    kasus: 'A',
                    beispiel: 'Karla bemüht sich um eine Arbeit.',
                }
            ],
            [
                {
                    verb: 'berichten',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Der Reporter berichtet über die Wahlen.',
                }
            ],
            [
                {
                    verb: 'sich beschäftigen',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Ich beschäftige mich gern mit Pflanzen.',
                }
            ],
            [
                {
                    verb: 'sich beschweren',
                    praposition: 'bei',
                    kasus: 'D',
                    beispiel: 'Der Gast beschwert sich beim Kellner.',
                }
            ],
            [
                {
                    verb: 'bestehen',
                    praposition: 'aus',
                    kasus: 'D',
                    beispiel: 'Eheringe bestehen aus Gold.',
                }
            ],
            [
                {
                    verb: 'bestehen',
                    praposition: 'auf',
                    kasus: 'D',
                    beispiel: 'Ich bestehe auf sofortiger Bezahlung des Autos.',
                }
            ],
            [
                {
                    verb: 'sich beteiligen',
                    praposition: 'an',
                    kasus: 'D',
                    beispiel: 'Viele Studenten beteiligen sich an den Streiks.',
                }
            ],
            [
                {
                    verb: 'sich bewerben (bei)',
                    praposition: 'bei',
                    kasus: 'D',
                    beispiel: 'Er bewirbt sich bei einer Bäckerei.',
                }
            ],
            [
                {
                    verb: 'sich bewerben (um)',
                    praposition: 'um',
                    kasus: 'A',
                    beispiel: 'Sie bewirbt sich um eine Stelle als Sekretärin.',
                }
            ],
            [
                {
                    verb: 'sich beziehen',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Meine Frage bezieht sich auf Ihr Angebot.',
                }
            ],
            [
                {
                    verb: 'bitten',
                    praposition: 'um',
                    kasus: 'A',
                    beispiel: 'Der Redner bittet um Aufmerksamkeit.',
                }
            ],
            [
                {
                    verb: 'danken',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Sam dankt für Ritas Hilfe.',
                }
            ],
            [
                {
                    verb: 'denken',
                    praposition: 'an',
                    kasus: 'A',
                    beispiel: 'Maria denkt oft an den Urlaub.',
                }
            ],
            [
                {
                    verb: 'diskutieren',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Das Kabinett diskutiert über eine neue Steuer.',
                }
            ],
            [
                {
                    verb: 'einladen',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'Ich lade dich zu meinem Geburtstag ein.',
                }
            ],
            [
                {
                    verb: 'sich entscheiden',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Kinder entscheiden sich gern für Schokolade.',
                }
            ],
            [
                {
                    verb: 'sich entschließen',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'Karl entschließt sich zu einem Studium.',
                }
            ],
            [
                {
                    verb: 'sich entschuldigen (bei)',
                    praposition: 'bei',
                    kasus: 'D',
                    beispiel: 'Tom entschuldigt sich bei ihrem Mann.',
                }
            ],
            [
                {
                    verb: 'sich entschuldigen (für)',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Ich entschuldige mich für das Verhalten meiner Katze.',
                }
            ],
            [
                {
                    verb: 'sich erholen ',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Von dem Schock muss ich mich erst erholen.',
                }
            ],
            [
                {
                    verb: 'sich erinnern ',
                    praposition: 'an',
                    kasus: 'A',
                    beispiel: 'Wir erinnern uns gern an unser erstes Ehejahr.',
                }
            ],
            [
                {
                    verb: 'erkennen ',
                    praposition: 'an',
                    kasus: 'D',
                    beispiel: 'Man erkennt Pinocchio an seiner langen Nase.',
                }
            ],
            [
                {
                    verb: 'sich erkundigen ',
                    praposition: 'nach',
                    kasus: 'D',
                    beispiel: 'Oma erkundigt sich oft nach meinen Plänen.',
                }
            ],
            [
                {
                    verb: 'erschrecken ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Der Koch erschrickt über eine Maus.',
                }
            ],
            [
                {
                    verb: 'erzählen ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Ein Ostberliner erzählt über sein Leben in der ehemaligen DDR.',
                }
            ],
            [
                {
                    verb: 'erzählen ',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Der Bischoff erzählt von der Reise nach Rom.',
                }
            ],
            [
                {
                    verb: 'fragen ',
                    praposition: 'nach',
                    kasus: 'D',
                    beispiel: 'Die Journalistin fragt nach den Konsequenzen der Gesetzesänderung.',
                }
            ],
            [
                {
                    verb: 'sich freuen ',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Kinder freuen sich auf die Ferien.',
                }
            ],
            [
                {
                    verb: 'sich freuen ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Jeder freut sich über eine Gehaltserhöhung.',
                }
            ],
            [
                {
                    verb: 'sich freuen ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Jeder freut sich über eine Gehaltserhöhung.',
                }
            ],
            [
                {
                    verb: 'gehen ',
                    praposition: 'um',
                    kasus: 'A',
                    beispiel: 'Immer geht es um Geld.',
                }
            ],
            [
                {
                    verb: 'gehören ',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'Das Elsass gehört zu Frankreich.',
                }
            ],
            [
                {
                    verb: 'sich gewöhnen ',
                    praposition: 'an',
                    kasus: 'A',
                    beispiel: 'Ich kann mich nicht an die Zeitumstellung gewöhnen.',
                }
            ],
            [
                {
                    verb: 'glauben ',
                    praposition: 'an',
                    kasus: 'A',
                    beispiel: 'Teenager glauben an die große Liebe.',
                }
            ],
            [
                {
                    verb: 'gratulieren ',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'Wir gratulieren dir zum 18. Geburtstag.',
                }
            ],
            [
                {
                    verb: 'halten ',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Ich halte das für keine gute Idee.',
                }
            ],
            [
                {
                    verb: 'halten ',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Kinder halten nicht viel von Ordnung.',
                }
            ],
            [
                {
                    verb: 'sich handeln ',
                    praposition: 'um',
                    kasus: 'A',
                    beispiel: 'Bei der Kopie handelt es sich nicht um Originalsoftware.',
                }
            ],
            [
                {
                    verb: 'handeln ',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Märchen handeln von Gut und Böse.',
                }
            ],
            [
                {
                    verb: 'helfen ',
                    praposition: 'bei',
                    kasus: 'D',
                    beispiel: 'Kann ich dir beim Tischdecken helfen?',
                }
            ],
            [
                {
                    verb: 'hindern ',
                    praposition: 'an',
                    kasus: 'D',
                    beispiel: 'Ein langsamer Fahrer hindert Greta am Überholen.',
                }
            ],
            [
                {
                    verb: 'hoffen ',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Im März hoffen alle auf warme Frühlingstage.',
                }
            ],
            [
                {
                    verb: 'hören ',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Ich habe seit Sonntag nichts von Piet gehört.',
                }
            ],
            [
                {
                    verb: 'sich informieren ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Auf der Messe kann man sich über die neue Technologie informieren.',
                }
            ],
            [
                {
                    verb: 'sich interessieren ',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Monika interessiert sich für ein Smartphone.',
                }
            ],
            [
                {
                    verb: 'klagen ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Tim klagt häufig über Kopfschmerzen.',
                }
            ],
            [
                {
                    verb: 'kämpfen ',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Die Gewerkschaft kämpft für höhere Löhne.',
                }
            ],
            [
                {
                    verb: 'kommen ',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'In der Besprechung kam es zu einem Streit.',
                }
            ],
            [
                {
                    verb: 'sich konzentrieren ',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Karl konzentriert sich auf seine Hausaufgaben.',
                }
            ],
            [
                {
                    verb: 'sich kümmern ',
                    praposition: 'um',
                    kasus: 'A',
                    beispiel: 'Im Pflegeheim kümmert man sich um alte Leute, die krank sind.',
                }
            ],
            [
                {
                    verb: 'lachen ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Über einen guten Witz muss man laut lachen.',
                }
            ],
            [
                {
                    verb: 'leiden (an)',
                    praposition: 'an',
                    kasus: 'D',
                    beispiel: 'Jeder fünfte Manager leidet an Burn-out.',
                }
            ],
            [
                {
                    verb: 'leiden (unter)',
                    praposition: 'unter',
                    kasus: 'D',
                    beispiel: 'Kaffeetrinker leiden unter Schlafproblemen.',
                }
            ],
            [
                {
                    verb: 'nachdenken ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Beamte müssen nicht über ihre Rente nachdenken.',
                }
            ],
            [
                {
                    verb: 'protestieren ',
                    praposition: 'gegen',
                    kasus: 'A',
                    beispiel: 'Viele Menschen protestieren gegen Atomkraft.',
                }
            ],
            [
                {
                    verb: 'rechnen ',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Im Januar muss man mit Schnee rechnen.',
                }
            ],
            [
                {
                    verb: 'reden (über)',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Deine Mutter redet gern über Krankheiten.',
                }
            ],
            [
                {
                    verb: 'reden (von)',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Großvater redet von den guten alten Zeiten.',
                }
            ],
            [
                {
                    verb: 'riechen ',
                    praposition: 'nach',
                    kasus: 'D',
                    beispiel: 'Hier riecht es nach Kuchen.',
                }
            ],
            [
                {
                    verb: 'sagen (über)',
                    praposition: 'über',
                    kasus: '',
                    beispiel: 'Brigitte sagt über Dietmar, dass er oft lügt.',
                }
            ],
            [
                {
                    verb: 'sagen (zu)',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'Was sagst du zu meinem neuen Haarschnitt?',
                }
            ],
            [
                {
                    verb: 'schicken (an)',
                    praposition: 'an',
                    kasus: 'A',
                    beispiel: 'Die E-Mail schicke ich dir morgen.',
                }
            ],
            [
                {
                    verb: 'schicken (zu)',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'Der Allgemeinmediziner schickt den Patienten zu einem Spezialisten.',
                }
            ],
            [
                {
                    verb: 'schimpfen ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Alle schimpfen über den Regen.',
                }
            ],
            [
                {
                    verb: 'schmecken ',
                    praposition: 'nach',
                    kasus: 'D',
                    beispiel: 'Muscheln schmecken nach Meerwasser.',
                }
            ],
            [
                {
                    verb: 'schreiben ',
                    praposition: 'an',
                    kasus: 'A',
                    beispiel: 'Bitte schreibe noch heute an deine Mutter.',
                }
            ],
            [
                {
                    verb: 'sich schützen ',
                    praposition: 'vor',
                    kasus: 'D',
                    beispiel: 'Den Computer muss man vor Hackern schützen.',
                }
            ],
            [
                {
                    verb: 'sein (für)',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Ich bin für die Abschaffung der Kinderarbeit.',
                }
            ],
            [
                {
                    verb: 'sein (gegen)',
                    praposition: 'gegen',
                    kasus: 'A',
                    beispiel: 'Viele sind gegen Steuererhöhungen.',
                }
            ],
            [
                {
                    verb: 'sorgen ',
                    praposition: 'für',
                    kasus: 'A',
                    beispiel: 'Kinder müssen im Alter für ihre Eltern sorgen.',
                }
            ],
            [
                {
                    verb: 'sprechen (mit)',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Ich spreche noch einmal mit deinem Vater.',
                }
            ],
            [
                {
                    verb: 'sprechen (über)',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Lass uns über deine Zukunft sprechen.',
                }
            ],
            [
                {
                    verb: 'sterben ',
                    praposition: 'an',
                    kasus: 'D',
                    beispiel: 'Zwei Deutsche sind an der Grippe gestorben.',
                }
            ],
            [
                {
                    verb: 'streiten (mit)',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Ich möchte nicht mit dir streiten.',
                }
            ],
            [
                {
                    verb: 'streiten (über)',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Die USA und Deutschland streiten über eine neue Strategie.',
                }
            ],
            [
                {
                    verb: 'teilnehmen ',
                    praposition: 'an',
                    kasus: 'D',
                    beispiel: 'Nordkorea nimmt an der Fußball-WM teil.',
                }
            ],
            [
                {
                    verb: 'telefonieren ',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Hast du schon mit dem Arzt telefoniert?',
                }
            ],
            [
                {
                    verb: 'sich treffen ',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Die Kanzlerin trifft sich täglich mit ihrem Pressesprecher.',
                }
            ],
            [
                {
                    verb: 'sich treffen ',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'Sie treffen sich nur zu einem kurzen Gespräch.',
                }
            ],
            [
                {
                    verb: 'überreden ',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'Kann ich dich zu einem Glas Wein überreden?',
                }
            ],
            [
                {
                    verb: 'sich unterhalten ',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Der Sänger unterhält sich mit dem Bassisten.',
                }
            ],
            [
                {
                    verb: 'sich unterhalten ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Die Modedesigner unterhalten sich über die neuesten Trends.',
                }
            ],
            [
                {
                    verb: 'sich verabreden ',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Heute verabrede ich mich mit einer Freundin.',
                }
            ],
            [
                {
                    verb: 'sich verabschieden ',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Nun wollen wir uns von euch verabschieden.',
                }
            ],
            [
                {
                    verb: 'vergleichen ',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Vergleichen Sie München mit Berlin.',
                }
            ],
            [
                {
                    verb: 'sich verlassen ',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Auf mich kann man sich verlassen.',
                }
            ],
            [
                {
                    verb: 'sich verlieben ',
                    praposition: 'in',
                    kasus: 'A',
                    beispiel: 'Britta hat sich in das alte Bauernhaus verliebt.',
                }
            ],
            [
                {
                    verb: 'sich verstehen ',
                    praposition: 'mit',
                    kasus: 'D',
                    beispiel: 'Daniel versteht sich gut mit seinem Chef.',
                }
            ],
            [
                {
                    verb: 'verstehen ',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Verstehst du etwas von Elektrik?',
                }
            ],
            [
                {
                    verb: 'sich vorbereiten ',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Karl bereitet sich auf eine Präsentation vor.',
                }
            ],
            [
                {
                    verb: 'warnen ',
                    praposition: 'vor',
                    kasus: 'D',
                    beispiel: 'Man hatte ihn vor den hohen Kosten für das alte Auto gewarnt.',
                }
            ],
            [
                {
                    verb: 'warten ',
                    praposition: 'auf',
                    kasus: 'A',
                    beispiel: 'Hier wartet man lange auf einen Bus.',
                }
            ],
            [
                {
                    verb: 'sich wenden ',
                    praposition: 'an',
                    kasus: 'A',
                    beispiel: 'Bitte wenden Sie sich an die Buchhaltung.',
                }
            ],
            [
                {
                    verb: 'werden ',
                    praposition: 'zu',
                    kasus: 'D',
                    beispiel: 'Unter null Grad wird Wasser zu Eis.',
                }
            ],
            [
                {
                    verb: 'wissen ',
                    praposition: 'von',
                    kasus: 'D',
                    beispiel: 'Ich weiß nichts von neuen Computern für unser Team.',
                }
            ],
            [
                {
                    verb: 'sich wundern ',
                    praposition: 'über',
                    kasus: 'A',
                    beispiel: 'Viele Deutsche wundern sich über die plötzlich so hohen Stromkosten.',
                }
            ],
            [
                {
                    verb: 'zuschauen ',
                    praposition: 'bei',
                    kasus: 'D',
                    beispiel: 'Kann ich dir bei der Reparatur zuschauen?',
                }
            ],
            [
                {
                    verb: 'zusehen ',
                    praposition: 'bei',
                    kasus: 'D',
                    beispiel: 'Willst du mir beim Kochen zusehen?',
                }
            ],
            [
                {
                    verb: 'zweifeln ',
                    praposition: 'an',
                    kasus: 'D',
                    beispiel: 'John zweifelt daran, dass sein Sohn die Wahrheit gesagt hat.',
                }
            ],
        ],
        // tasks chosen for next practise
        chosen: [],
        // mode for training - 'practice', 'exam'
        mode: '',
    },
    reducers: {
        chooseTasks: (state, action) => {
            // will select random number of tasks and assign it to 'chosen' state
            const nrOfTasks = action.payload;
            let allTasks = [...state.all];

            for (let i = 0; i < nrOfTasks; i++) {
                let taskNr = Math.floor(Math.random() * allTasks.length);

                let next = allTasks.splice(taskNr, 1);

                state.chosen.push(next);
            }
        },
        setMode: (state, action) => {
            state.mode = action.payload;
        },
        zeroTasks: (state, action) => {
            state.chosen = [];
        }
    }
})

// Action creators are generated for each case reducer function
export const { chooseTasks, setMode, zeroTasks } = verbenMitPrapositionenSlice.actions

export default verbenMitPrapositionenSlice.reducer