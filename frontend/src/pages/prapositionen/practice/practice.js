import React, { Component } from 'react';
import { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { ReactReduxContext } from 'react-redux';
import { useSelector, useDispatch } from 'react-redux';
import { chooseTasks } from '../verben-mit-prapositionenSlice';
import './practice.scss';
import Button from '../../../components/button/button';

const Practise = () => {
    // get values from redux state
    const allTasks = useSelector(state => state.verbenMitPrapositionen.all);
    const chosenTasks = useSelector(state => state.verbenMitPrapositionen.chosen);
    const mode = useSelector(state => state.verbenMitPrapositionen.mode);   // string with values => "practice-mode", "exam-mode"
    const dispatch = useDispatch();
    const navigate = useNavigate();

    // local state
    const [score, setScore] = useState(0);
    const [checked, setChecked] = useState(false);
    const [verb, setVerb] = useState('');
    const [tasks, setTasks] = useState(chosenTasks);
    const [taskNr, setTaskNr] = useState(-1);
    const [praposition, setPraposition] = useState(['']);
    const [kasus, setKasus] = useState(['']);
    const [enteredPraposition, setEnteredPraposition] = useState(['']);
    const [enteredKasus, setEnteredKasus] = useState(['']);
    const [beispiel, setBeispiel] = useState(['']);
    const [classPraposition, setClassPraposition] = useState('input-text');
    const [classKasus, setClassKasus] = useState('input-text');
    const [btnCheckClass, setBtnCheckClass] = useState('button btn-hidden');
    const [btnNextClass, setBtnNextClass] = useState('button');
    const [btnNextValue, setBtnNextValue] = useState('START');

    // hooks
    useEffect(() => {

    }, []);

    // action
    const btnCheck_Click = () => {
        // console.log('CHECK clicked...');

        // color for input fiels
        praposition === enteredPraposition.toString().toLowerCase() ? setClassPraposition('input-text green') : setClassPraposition('input-text red');
        kasus === enteredKasus ? setClassKasus('input-text green') : setClassKasus('input-text red');
        // update score
        if (praposition === enteredPraposition && kasus === enteredKasus) {
            setScore(score + 1);

            if (mode === 'practice-mode') { removeTask(taskNr); }
        }

        if (mode === 'exam-mode') { removeTask(taskNr); }

        // 
        setChecked(true);

        // change button visibility
        if (tasks.length > 0) setBtnNextClass('button');
        setBtnCheckClass('button btn-hidden');
    }

    const btnNext_Click = () => {
        if (tasks.length > 0) {
            // clear input fiels
            setEnteredPraposition('');
            setEnteredKasus('');

            // set input fields classes
            setClassPraposition('input-text');
            setClassKasus('input-text');

            // get next task -> array
            let nextTask = getTask();

            // console.log('nextTask: ', nextTask);
            setVerb(nextTask.verb);
            setPraposition(nextTask.praposition);
            setKasus(nextTask.kasus);
            setBeispiel(nextTask.beispiel);

            // hide result
            setChecked(false);

            // change button visibility
            setBtnNextValue('NEXT');
            setBtnNextClass('button btn-hidden');
            setBtnCheckClass('button');
        }
    }

    const getTask = () => {
        if (tasks.length > 0) {
            let number = Math.floor(Math.random() * tasks.length)

            // returns number between 0 .... tasks.length - 1 
            setTaskNr(number);

            return tasks[number][0][0];
        }
    }

    const removeTask = (number) => {
        let currTasks = [...tasks];

        currTasks.splice(number, 1);

        setTasks(currTasks);
    }

    const btnBackToSettings_Click = () => { navigate("/prapositionen/"); }
    const btnBackToMainMenu_Click = () => { navigate("/"); }

    // view
    return (<div className='practise'>
        <div className='faked-opacity'></div>
        <div className='header'>
            <div className='navbar'>
                <Button color='blue' value='<< PRÄPOSITIONEN' onClick={btnBackToSettings_Click} />
            </div>
        </div>
        <div className='main'>
            <div className='main-header'>
                <div className='text'>Exercise <em>"Verben mit Präpositionen"</em>:</div>
            </div>
            <div className='content'>
                <div className='row'>
                    <div className='group'>
                        <div className='cell'>Verb</div>
                        <div className='cell'>{verb}</div>
                    </div>
                    <div className='group'>
                        <div className='cell'>Präposition</div>
                        <div className='cell'>
                            <input className={classPraposition} type='text' value={enteredPraposition} onChange={(e) => setEnteredPraposition(e.target.value)} />
                        </div>
                    </div>
                    <div className='group'>
                        <div className='cell'></div>
                        <div className='cell cell-plus'>+</div>
                    </div>
                    <div className='group'>
                        <div className='cell'>Kasus</div>
                        <div className='cell'>
                            <input className={classKasus} type='text' value={enteredKasus} onChange={(e) => setEnteredKasus(e.target.value)} />
                        </div>
                    </div>
                </div>
                <div className={checked ? 'answer' : 'answer answer-hidden'}>{beispiel}</div>
            </div>
        </div>
        <footer className='footer'>
            <div className='content-info'>
                <div className='info'>Score: {score} / {chosenTasks.length}</div>
                <div className='info'>remaining: {tasks.length} ({mode})</div>
            </div>
            <div className='content'>
                <input className={btnCheckClass} type='button' value='CHECK' onClick={btnCheck_Click} />
                <input className={btnNextClass} type='button' value={btnNextValue} onClick={btnNext_Click} />
            </div>
        </footer>
        {/* <div className='temporary for debugging...'>
            <strong>Redux state:</strong>
            <div onClick={() => { console.log('allTasks: ', allTasks) }}>allTasks</div>
            <div onClick={() => { console.log('chosenTasks: ', chosenTasks) }}>chosenTasks</div>
            <strong>Local state of component:</strong>
            <div onClick={() => { console.log('tasks: ', tasks) }}>tasks</div>
            <div onClick={() => { console.log('mode: ', mode) }}>mode</div>
        </div> */}
    </div>);
}

export default Practise;