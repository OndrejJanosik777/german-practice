import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { store } from './app/store';
// pages
import LandingPage from './pages/landing-page/landing-page';
import Prapositionen from './pages/prapositionen/prapositionen';
import Practice from './pages/prapositionen/practice/practice';
import Overview from './pages/prapositionen/overview/overview';
import reportWebVitals from './reportWebVitals';
import './index.css';

const container = document.getElementById('root');
const root = createRoot(container);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/prapositionen/" element={<Prapositionen />} />
          <Route path="/prapositionen/practice/" element={<Practice />} />
          <Route path="/prapositionen/overview/" element={<Overview />} />

        </Routes>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
